# Use an OpenJDK runtime as a parent image
FROM openjdk:11-jre-slim
# Set the working directory to /app
WORKDIR /app
# Copy the compiled Spring Boot JAR file into the container
COPY $CI_PROJECT_DIR/ems/target/spring-ems-2.7.0-SNAPSHOT.jar /app/app.jar
# Make port 8080 available to the world outside this container
EXPOSE 8080
# Run the Spring Boot application
CMD ["java", "-jar", "app.jar"]
